function DPhiR = DPhiReac(u1, u2, dt, reacJac)
%PHIREAC Jacobian of the reaction part
    
    DPhiR = -dt*reacJac(u1,u2);

end


