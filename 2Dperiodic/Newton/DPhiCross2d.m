function DPhiC = DPhiCross2d(u1, u2, muC1, muC2, rho1Conv, rho2Conv, Ax, Ay, dx, dy, dt, d12, d21, doSparse)
%DPHICROSS2D Jacobian of the cross-diffusion part

    ALapl = dt*(Ax/(dx*dx)+Ay/(dy*dy));
    ALapl2 = ALapl*dx*dy;
    
    if doSparse
        %%% Species 1 differentiated wrt species 1 
        DPhiC11 = d12*ALapl*spdiags(muC1,0,length(muC1),length(muC1));
        %%% Species 2 differentiated wrt species 2 
        DPhiC22 = d21*ALapl*spdiags(muC2,0,length(muC2),length(muC2));
        %%% Species 1 differentiated wrt species 2
        DPhiC12 = d12*ALapl2*spdiags(u1,0,length(u1),length(u1))*rho1Conv;
        %%% Species 2 differentiated wrt species 1
        DPhiC21 = d21*ALapl2*spdiags(u2,0,length(u2),length(u2))*rho2Conv;
    else
        %%% Species 1 differentiated wrt species 1 
        DPhiC11 = d12*ALapl*diag(muC1);
        %%% Species 2 differentiated wrt species 2 
        DPhiC22 = d21*ALapl*diag(muC2);
        %%% Species 1 differentiated wrt species 2
        DPhiC12 = d12*ALapl2*diag(u1)*rho1Conv;
        %%% Species 2 differentiated wrt species 1
        DPhiC21 = d21*ALapl2*diag(u2)*rho2Conv;
    end
    

    DPhiC = [[DPhiC11,DPhiC12];[DPhiC21,DPhiC22]];
end

