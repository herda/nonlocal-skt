function [] = checkJacobian2d(showNonZeroCoefficients, RHO1, RHO2, SIG1, SIG2, rho1Conv, rho2Conv, sig1Conv, sig2Conv, Ax, Ay, dx, dy, dt, Nx, Ny, N, d12, d21, d11, d22, d1, d2, reacfunc, reacJac, doSparse)
%CHECKJACOBIAN Checks the Taylor expansion to see if the Jacobian is well
%computed. Can show which coefficients are wrong in the matrix. 

%% Checking Jacobian
    u = rand(2*N,1);
    un = rand(2*N,1);
    [P,muC1,muC2, muS1, muS2] = Phi2d(un, u, RHO1, RHO2, SIG1, SIG2, Ax, Ay, dx, dy, dt, Nx, Ny, d12, d21, d11, d22, d1, d2, reacfunc);
    DP = DPhi2d(u, muC1, muC2, muS1, muS2, rho1Conv, rho2Conv, sig1Conv, sig2Conv, Ax, Ay, dx, dy, dt, N, d12, d21, d11, d22,d1,d2,reacJac, doSparse);
    a = rand(2*N,1); 
    h = 1e-3;
    Norold = Inf;
    disp('-------------------')
    disp('Checking order 2 in Taylor expansion of F(U+ha) - F(u) - h JF(u)a:')
    disp('-------------------')
    while h>1e-6 %&& Norold > 1e-12
        [Ph,~,~,~,~] = Phi2d(un, u+h*a, RHO1, RHO2, SIG1, SIG2, Ax, Ay, dx, dy, dt, Nx, Ny, d12, d21, d11, d22, d1, d2, reacfunc);
        Nor = norm(Ph - P - DP*a*h,Inf);
        disp(['For h = ', num2str(h), ' error is ',num2str(Nor), ' and experimental order is ',  num2str((log(Nor) - log(Norold))/(log(h) - log(4*h)))]) %%% should be ~2 if correct
        h = h/4;
        Norold = Nor;
    end
    
    
%     %% Show wrong coefficents 
%     
%     if showNonZeroCoefficients
%         figure(10)
%         JFapprox = [];
%         h = 1e-6;
%         for i = 1:dim
%             a = zeros(q*dim,1);
%             a(i) = 1;
%             Fh =  Func(u+h*a,un,1,.1,1);
%             JFapprox = [JFapprox ,(Fh - F)/h];
%         end
%         spy(abs((JF-JFapprox)/norm(JF, Inf))>=1e-6);
%         set(gca,'fontsize', 12,'TickLabelInterpreter', 'latex')
%         title('Possibly wrong Jacobian coefficients (none if blank)','Interpreter','latex')
%         drawnow
%     end

% %% Test de calcul de la Jacobienne
% u = rand(2*N,1);
% un = rand(2*N,1);
% P = Phi(u,un,rho,dx,N,CFLp,d12,d21);
% DP =  DPhi(u,rho,dx,N,CFLp,CFLh,d12,d21);
% a = rand(2*N,1); 
% h = 1;
% Norold = Inf;
% while h>1e-12 && Norold > 1e-12
%     Ph = Phi(u+h*a,un,rho,dx,N,CFLp,d12,d21);
%     Nor = norm(Ph - P - DP*a*h,2);
%     disp((log(Nor) - log(Norold))/(log(h) - log(2*h))) %%% should be ~2 if correct
%     h = h/2;
%     Norold = Nor;
%     pause(.1)
% end
% 
% %% Calcul de la Jacobienne approchée pour comparaison
% 
% DPapprox = [];
% h = 1e-12;
% for i = 1:2*N
%     a = zeros(2*N,1);
%     a(i) = 1;
%     Ph = Phi(u+h*a,un,rho,dx,N,CFLp,d12,d21);
%     DPapprox = [DPapprox ,(Ph - P)/h];
% end
% 
% spy(abs(DP-DPapprox)>=1e-3);
% 

end
