function DPhiD = DPhiDiff2d(Ax, Ay, dx, dy, dt, d1, d2)
%DPHIDIFF2D Jacobian of the linear diffusion part

    ALapl = dt*(Ax/(dx*dx)+Ay/(dy*dy));
    DPhiD = blkdiag(d1*ALapl,d2*ALapl);

end

