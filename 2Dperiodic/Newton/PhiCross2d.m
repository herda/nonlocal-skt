function [PhiC,muC1,muC2] = PhiCross2d(u1, u2, U1, U2, RHO1, RHO2, Ax, Ay, dx, dy, dt, N, d12, d21)
%PHICROSS2D Function of the cross-diffusion part

    [MUC1,MUC2] = ConvCross2d(U1,U2,RHO1,RHO2,dx,dy);
    muC1 = reshape(MUC1', N, 1);
    muC2 = reshape(MUC2', N, 1);
    
    ALapl = dt*(Ax/(dx*dx)+Ay/(dy*dy));
    PhiC = [d12*ALapl*(u1.*muC1); d21*ALapl*(u2.*muC2)];
end

