function [PhiS,muS1,muS2] = PhiSelf2d(u1, u2, U1, U2, SIG1, SIG2, Ax, Ay, dx, dy, dt, N, d11, d22)
%PHISELF2D Function of the self-diffusion part

    [MUS1,MUS2] = ConvSelf2d(U1,U2,SIG1,SIG2,dx,dy);
    muS1 = reshape(MUS1', N, 1);
    muS2 = reshape(MUS2', N, 1);
    
    ALapl = dt*(Ax/(dx*dx)+Ay/(dy*dy));
    PhiS = [d11*ALapl*(u1.*muS1); d22*ALapl*(u2.*muS2)];

end

