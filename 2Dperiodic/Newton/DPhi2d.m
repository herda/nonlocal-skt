function DP = DPhi2d(u, muC1, muC2, muS1, muS2, rho1Conv, rho2Conv, sig1Conv, sig2Conv, Ax, Ay, dx, dy, dt, N, d12, d21, d11, d22,d1,d2,reacJac, doSparse)
%DPHI Jacobian of the whole scheme

    u1 = u(1:N);
    u2 = u((N+1):end);
    
    DPhiC = DPhiCross2d(u1, u2, muC1, muC2, rho1Conv, rho2Conv, Ax, Ay, dx, dy, dt, d12, d21, doSparse);
    DPhiS = DPhiSelf2d(u1, u2, muS1, muS2, sig1Conv, sig2Conv, Ax, Ay, dx, dy, dt, d11, d22, doSparse);
    DPhiD = DPhiDiff2d(Ax, Ay, dx, dy, dt, d1, d2);
    DPhiR = DPhiReac(u1, u2, dt, reacJac);
    
    if doSparse
        DP = speye(length(u)) + DPhiC + DPhiS + DPhiD + DPhiR;
    else
        DP = eye(length(u)) + DPhiC + DPhiS + DPhiD + DPhiR;
    end
    
end

