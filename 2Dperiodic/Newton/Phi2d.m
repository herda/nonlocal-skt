function [P,muC1,muC2, muS1, muS2] = Phi2d(un, u, RHO1, RHO2, SIG1, SIG2, Ax, Ay, dx, dy, dt, Nx, Ny, d12, d21, d11, d22, d1, d2, reacfunc)
%PHI2d Nonlinear function defining the scheme

    N = Nx*Ny;

    u1 = u(1:N);
    u2 = u(N+1:end);

    U1 = reshape(u1,Nx,Ny)';
    U2 = reshape(u2,Nx,Ny)';
    
    [PhiC,muC1,muC2] = PhiCross2d(u1, u2, U1, U2, RHO1, RHO2, Ax, Ay, dx, dy, dt, N, d12, d21);
    [PhiS,muS1,muS2] = PhiSelf2d(u1, u2, U1, U2, SIG1, SIG2, Ax, Ay, dx, dy, dt, N, d11, d22);
    PhiD = PhiDiff2d(u1, u2, Ax, Ay, dx, dy, dt, d1, d2);
    PhiR = PhiReac(u1, u2, dt, reacfunc);

    P = u + PhiS + PhiC + PhiD + PhiR - un;

end

