function PhiD = PhiDiff2d(u1, u2, Ax, Ay, dx, dy, dt, d1, d2)
%PHIDIFF2D Function of the linear diffusion part

   ALapl = dt*(Ax/(dx*dx)+Ay/(dy*dy));
   PhiD = [d1*ALapl*u1; d2*ALapl*u2];

end

