function [] = checkSparsefunc2d(RHO1, RHO2, SIG1, SIG2, rho1Conv, rho2Conv, sig1Conv, sig2Conv, Ax, Ay, dx, dy, dt, Nx, Ny, N, d12, d21, d11, d22, d1, d2, reacfunc, reacJac, doSparse)
%CHECKSPARSEFUNC Checks if the Jacobian is a sparse matrix

disp('-------------------')
if doSparse
    u = rand(2*N,1);
    un = rand(2*N,1);
    [~,muC1,muC2, muS1, muS2] = Phi2d(un, u, RHO1, RHO2, SIG1, SIG2, Ax, Ay, dx, dy, dt, Nx, Ny, d12, d21, d11, d22, d1, d2, reacfunc);
    DP = DPhi2d(u, muC1, muC2, muS1, muS2, rho1Conv, rho2Conv, sig1Conv, sig2Conv, Ax, Ay, dx, dy, dt, N, d12, d21, d11, d22,d1,d2,reacJac, doSparse);
    if issparse(DP)
        disp('The sparse matrix option is activated and the Jacobian is indeed sparse')
    else
        disp('Warning: The sparse matrix option is activated but the Jacobian is not sparse')
    end
else
    disp('The sparse matrix option is not activated')
end

end

