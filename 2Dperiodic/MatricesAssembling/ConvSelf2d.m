function [MU1S, MU2S] = ConvSelf2d(U1,U2,SIG1,SIG2,dx,dy)
%CONVSELF2D  computes the convolution products in the self-diffusion term

    MU1S = dx * dy * ifft2(fft2(SIG1).*fft2(U1));
    MU2S = dx * dy * ifft2(fft2(SIG2).*fft2(U2));

end

