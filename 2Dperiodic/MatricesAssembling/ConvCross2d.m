function [MU1C,MU2C] = ConvCross2d(U1,U2,RHO1,RHO2,dx,dy)
%CONVCROSS2D computes the convolution products in the cross-diffusion term

    MU1C = dx * dy * ifft2(fft2(RHO1).*fft2(U2));
    MU2C = dx * dy * ifft2(fft2(RHO2).*fft2(U1));

end

