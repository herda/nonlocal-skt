function [Ax, Ay, rho1Conv, rho2Conv, sig1Conv, sig2Conv]...
    = Assemble2d(Nx, Ny, rho1, rho2, sig1, sig2, RHO1, RHO2, SIG1, SIG2,  UseParallel, doSparse)
%ASSEMBLE2D Assembles matrices which are going to be used repeatedly in the
%computations
%
%   INPUT:
%
%   Nx*Ny: size of the vector of unknowns
%   rho1: cross convolution kernel for species 1
%   rho2: cross convolution kernel for species 2
%   sig1: self convolution kernel for species 1
%   sig2: self convolution kernel for species 2
%   RHO1, RHO2, SIG1, SIG2: Matrix forms of the convolution kernels
%   UseParallel: parallelizes the assembling (option of func2mat routine)
%   doSparse: uses sparse matrices
%
%   OUTPUT:
%
%   A: discrete Laplacian with periodic BC
%   rho1Conv: discrete convolution with rho1
%   rho2Conv: discrete convolution with rho2
%   sig1Conv: discrete convolution with sig1
%   sig2Conv: discrete convolution with sig2


%%% Matrix of the Laplacian assembled with fft
N = Nx*Ny;
multiplx = 4*diag(sin(pi*(0:(N-1))/N).^2);
multiply = 4*diag(sin(pi*(0:(N-1))*Nx/N).^2);
Ax = round(real(ifft(multiplx*fft(eye(N)))));
Ay = round(real(ifft(multiply*fft(eye(N)))));
if doSparse
    Ax = sparse(Ax);
    Ay = sparse(Ay);
end


%%% Matrices of convolution kernels assembled with fft
tol = 1e-12;

rho1Conv = @(X) reshape(real(ifft2(fft2(RHO1).*fft2(reshape(X,Nx,Ny)')))',N,1); 
rho1Conv = @(X) rho1Conv(X).*(abs(rho1Conv(X))>tol);
rho1Conv = func2mat(rho1Conv,rho1,'UseParallel',UseParallel, 'doSparse', doSparse);

rho2Conv = @(X) reshape(real(ifft2(fft2(RHO2).*fft2(reshape(X,Nx,Ny)')))',N,1); 
rho2Conv = @(X) rho2Conv(X).*(abs(rho2Conv(X))>tol);
rho2Conv = func2mat(rho2Conv,rho2,'UseParallel',UseParallel, 'doSparse', doSparse);

sig1Conv = @(X) reshape(real(ifft2(fft2(SIG1).*fft2(reshape(X,Nx,Ny)')))',N,1); 
sig1Conv = @(X) sig1Conv(X).*(abs(sig1Conv(X))>tol);
sig1Conv = func2mat(sig1Conv,sig1,'UseParallel',UseParallel, 'doSparse', doSparse);

sig2Conv = @(X) reshape(real(ifft2(fft2(SIG2).*fft2(reshape(X,Nx,Ny)')))',N,1); 
sig2Conv = @(X) sig2Conv(X).*(abs(sig2Conv(X))>tol);
sig2Conv = func2mat(sig2Conv,sig2,'UseParallel',UseParallel, 'doSparse', doSparse);


end

