function out = ReacLV(u1,u2,a1,a2,b1,b2,Jac,doSparse)
%REACLV Lotka Volterra reaction term

if Jac % Evaluation of the Jacobian of the function
    N = length(u1);
    if doSparse
        out = [[a1*speye(N) - b1*spdiags(u2,0,N,N), -b1*spdiags(u1,0,N,N)]; [b2*spdiags(u2,0,N,N), -a2*speye(N) + b2*spdiags(u1,0,N,N)]];
    else
        out = [[a1*eye(N) - b1*diag(u2), -b1*diag(u1)]; [b2*diag(u2), -a2*eye(N) + b2*diag(u1)]];
    end
else % Evaluation of the function
    out = [a1*u1 - b1*u1.*u2; -a2*u2 + b2*u1.*u2];
end

end

