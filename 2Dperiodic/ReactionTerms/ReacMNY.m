function out = ReacMNY(u1,u2,a,b,c,d,e,f,g,Jac,doSparse)
%REACMNY Mimura Nishiura Yamaguti 


if Jac % Evaluation of the Jacobian of the function
    N = length(u1);
    if doSparse
        out = [[a*speye(N)+2*e*spdiags(u1,0,N,N)-3*d*spdiags(u1.*u1,0,N,N)-b*spdiags(u2,0,N,N), -b*spdiags(u1,0,N,N)]; [c*spdiags(u2,0,N,N), -f*speye(N)-2*g*spdiags(u2,0,N,N)+c*spdiags(u1,0,N,N)]];
    else
        out = [[a*eye(N)+2*e*diag(u1)-3*d*diag(u1.*u1)-b*diag(u2), -b*diag(u1)]; [c*diag(u2), -f*eye(N)-2*g*diag(u2)+c*diag(u1)]];
    end
else % Evaluation of the function
    out = [a*u1 + e*u1.*u1 - d*u1.*u1.*u1- b*u1.*u2; -f*u2-g*u2.*u2 + c*u1.*u2];
end

end

