function out = ReacGLV(u1,u2,a1,a2,b1,b2, k1,k2, A,Jac,doSparse)
%REACGLV Generalized Lotka Volterra reaction term
f = @(x) k1 + a1*x.*(1-x/A); 
df = @(x) a1 - 2*x*a1/A;
g = @(x) a2*x-k2;
dg = @(x) a2 + 0*x;

if Jac % Evaluation of the Jacobian of the function
    N = length(u1);
    if doSparse
        out = [[spdiags(df(u1),0,N,N) - b1*spdiags(u2,0,N,N), -b1*spdiags(u1,0,N,N)]; [b2*spdiags(u2,0,N,N), -spdiags(dg(u2),0,N,N) + b2*spdiags(u1,0,N,N)]];
    else
        out = [[diag(df(u1)) - b1*diag(u2), -b1*diag(u1)]; [b2*diag(u2), -diag(dg(u2)) + b2*diag(u1)]];
    end
else % Evaluation of the function
    out = [f(u1) - b1*u1.*u2; -g(u2) + b2*u1.*u2];
end

end

