function [d1,d2,d12,d21,d11,d22, rho1func, rho2func, sig1func, sig2func, reacfunc, reacJac, u1func, u2func, description] = loadTestcase2d(testcase,Lx,Ly,doSparse)
%LOADTESTCASE Loads the parameters of the system of equations
%
%   INPUT:
%
%   testcase: identification number of the example (see below in the code of this function for details)
%   L: size of the domain
%   doSparse: option to use only sparse matrices (recommanded)
%
%   OUTPUT:
%
%   d1,d2,d12,d21,d11,d22: diffusion coefficients
%   rho1func, rho2func, sig1func, sig2func: convolution kernels
%   reacfunc, reacJac: function for reaction term and its Jacobian
%   u1func, u2func: initial data


disp('***************************')
disp(['Test case ',num2str(testcase)])

%%% Default values (2 uncoupled heat equations)


description = "Two decoupled heat equations\n";
d1=1;d2=1;d12=0;d21=0;d11=0;d22=0; % Only linear diffusion

%Indicatrix of a circle (wid = 0 is Dirac)
nr = @(x,y) sqrt(x.*x + y.*y);
indic = @(x,y,wid) (nr(x,y)<=wid) | (nr(Lx-x,y)<=wid) | (nr(Lx-x,Ly-y)<=wid) | (nr(x,Ly-y)<=wid);

rho1func = @(x,y,wid) indic(x,y,0); % Dirac
rho2func = @(x,y,wid) indic(x,y,0); % Dirac
sig1func = @(x,y,wid) indic(x,y,0); % Dirac
sig2func = @(x,y,wid) indic(x,y,0); % Dirac
reacfunc = @(u1,u2) 0*[u1;u2]; % No reaction
if doSparse
    reacJac = @(u1,u2) 0*spdiags([u1;u2],0,length([u1;u2]),length([u1;u2])); % No reaction
else
    reacJac = @(u1,u2) 0*diag([u1;u2]); % No reaction    
end
u1func = @(x,y) (x<=Lx/2)+0.*y;
u2func = @(x,y) (y<=Ly/2)+0.*x;


%%% Testcases
switch testcase
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 1 % Turing instability linear diffusion
        
        description = "Turing instability in Segel-Levin with linear diffusion\n";

        %%% Coefficients
        % Diffusion coefficients
        d1 = .05;
        d2 = 1;
        % Cross-diffusion coefficients
        d12 = 0;
        d21 = 0;
        % Self-diffusion coefficients
        d11 = 0;
        d22 = 0;

       
        %%% Reaction term
        a = 1; % birth rate for preys (linear term)
        b = 1; % death rate for preys per predator
        c = 1; % birth rate for predators per prey
        d = 2; % birth rate for prey (quadratic term)
        e = 1/2.5; % death rate for predators (quadratic term)


        reacfunc = @(u1,u2) ReacSL(u1,u2,a,b,c,d,e,0);
        reacJac = @(u1,u2) ReacSL(u1,u2,a,b,c,d,e,1,doSparse);

        eta = .1;
        perturb1 = @(x,y)  ((x.*y>= Lx/9).*(x.*y<=8*Lx/9));%.*((y>= Lx/9).*(y<=8*Lx/9));
        perturb2 = @(x,y)  rand(size(x));%%

        u1func = @(x,y) (a*d)/(b*c - d*e) + eta*perturb2(x,y);
        u2func = @(x,y) (a*c)/(b*c - d*e) + 0*perturb1(x,y);
        
        
        
    case 2 % Turing instability linear diffusion
        
        description = "Turing instability in Mimura Nishiura Yamaguti  with linear diffusion\n";

        %%% Coefficients
        % Diffusion coefficients
        d1 = .001;
        d2 = 4;
        % Cross-diffusion coefficients
        d12 = 0;
        d21 = 0;
        % Self-diffusion coefficients
        d11 = 0;
        d22 = 0;

       
        %%% Reaction term
       
        a = 35/9;
        b = 1;
        c = 1;
        d = 1/9;
        e = 16/9;
        f = 1;
        g = 2/5;

        reacfunc = @(u1,u2) ReacMNY(u1,u2,a,b,c,d,e,f,g,0,doSparse);
        reacJac = @(u1,u2) ReacMNY(u1,u2,a,b,c,d,e,f,g,1,doSparse);

        eta = .01;
        perturb = @(x,y)   rand(size(x));%((x>= Lx/9).*(x<=4*Lx/9)).*((y>= 7*Ly/9).*(y<=8*Ly/9)) + 0.;
        
        u1eq =  (f + (c*(b^2*c^2 - 2*b*c*e*g + 4*d*f*b*g + e^2*g^2 + 4*a*d*g^2)^(1/2) - b*c^2 + c*e*g - 2*d*f*g)/(2*d*g))/c;
        u2eq = (c*(b^2*c^2 - 2*b*c*e*g + 4*d*f*b*g + e^2*g^2 + 4*a*d*g^2)^(1/2) - b*c^2 + c*e*g - 2*d*f*g)/(2*d*g^2);

        u1func = @(x,y) u1eq + eta*perturb(x,y);
        u2func = @(x,y) u2eq - 0*perturb(Lx-x,Ly-y); 
        
        
        
    case 3 % Turing instability non-local cross diffusion
        
        description = "Turing instability in Mimura Nishiura Yamaguti  with nonlocal cross diffusion\n";

        %%% Coefficients
        % Diffusion coefficients
        d1 = .001;
        d2 = 0;
        % Cross-diffusion coefficients
        d12 = 0;
        d21 = 4/10;
        % Self-diffusion coefficients
        d11 = 0;
        d22 = 0;

       
        %%% Reaction term
       
        a = 35/9;
        b = 1;
        c = 1;
        d = 1/9;
        e = 16/9;
        f = 1;
        g = 2/5;

        reacfunc = @(u1,u2) ReacMNY(u1,u2,a,b,c,d,e,f,g,0,doSparse);
        reacJac = @(u1,u2) ReacMNY(u1,u2,a,b,c,d,e,f,g,1,doSparse);

        
        %%% Convolution kernels
        % Cross diffusion
        radint = min(Lx,Ly)/8;
        radext =  min(Lx,Ly)/6;
        %predatorfunc = @(r) r.^2.*(r<= rad) + (r-2*rad).^2.*(r>rad).*(r<=2*rad); % rad<= L/4
        predatorfunc = @(r) (r>= radint).*(r<=radext)*1.0;
        rho2func = @(x,y) predatorfunc(x.^2+y.^2).*(x<Lx/2).*(y<Ly/2) + predatorfunc((x-Lx).^2+y.^2).*(x>=Lx/2).*(y<Ly/2) + predatorfunc((x-Lx).^2+(y-Ly).^2).*(x>=Lx/2).*(y>=Ly/2) + predatorfunc(x.^2+(y-Ly).^2).*(x<Lx/2).*(y>=Ly/2);
        
        eta = .01;
        perturb = @(x,y)   ((x>= Lx/9).*(x<=4*Lx/9)).*((y>= 7*Ly/9).*(y<=8*Ly/9)) + 0.;
        
        u1eq =  (f + (c*(b^2*c^2 - 2*b*c*e*g + 4*d*f*b*g + e^2*g^2 + 4*a*d*g^2)^(1/2) - b*c^2 + c*e*g - 2*d*f*g)/(2*d*g))/c;
        u2eq = (c*(b^2*c^2 - 2*b*c*e*g + 4*d*f*b*g + e^2*g^2 + 4*a*d*g^2)^(1/2) - b*c^2 + c*e*g - 2*d*f*g)/(2*d*g^2);

        u1func = @(x,y) u1eq + eta*perturb(x,y);
        u2func = @(x,y) u2eq - 0*perturb(Lx-x,Ly-y); 
        
    case 4 % Turing instability non-local cross diffusion
        
        description = "Turing instability in Mimura Nishiura Yamaguti  with nonlocal cross diffusion and non symmetric convolution kernel \n";

        %%% Coefficients
        % Diffusion coefficients
        d1 = .001;
        d2 = 0;
        % Cross-diffusion coefficients
        d12 = 0;
        d21 = 4/10;
        % Self-diffusion coefficients
        d11 = 0;
        d22 = 0;

       
        %%% Reaction term
       
        a = 35/9;
        b = 1;
        c = 1;
        d = 1/9;
        e = 16/9;
        f = 1;
        g = 2/5;

        reacfunc = @(u1,u2) ReacMNY(u1,u2,a,b,c,d,e,f,g,0,doSparse);
        reacJac = @(u1,u2) ReacMNY(u1,u2,a,b,c,d,e,f,g,1,doSparse);

        
        %%% Convolution kernels
        % Cross diffusion
        radint = min(Lx,Ly)/8;
        radext =  min(Lx,Ly)/6;
        %predatorfunc = @(r) r.^2.*(r<= rad) + (r-2*rad).^2.*(r>rad).*(r<=2*rad); % rad<= L/4
        predatorfunc = @(r) (r>= radint).*(r<=radext)*1.0;
        rho2func = @(x,y) predatorfunc(x.^2+y.^2).*(x<Lx/2).*(y<Ly/2);
        
        eta = .01;
        perturb = @(x,y)   ((x>= Lx/9).*(x<=4*Lx/9)).*((y>= 7*Ly/9).*(y<=8*Ly/9)) + 0.;
        
        u1eq =  (f + (c*(b^2*c^2 - 2*b*c*e*g + 4*d*f*b*g + e^2*g^2 + 4*a*d*g^2)^(1/2) - b*c^2 + c*e*g - 2*d*f*g)/(2*d*g))/c;
        u2eq = (c*(b^2*c^2 - 2*b*c*e*g + 4*d*f*b*g + e^2*g^2 + 4*a*d*g^2)^(1/2) - b*c^2 + c*e*g - 2*d*f*g)/(2*d*g^2);

        u1func = @(x,y) u1eq + eta*perturb(x,y);
        u2func = @(x,y) u2eq - 0*perturb(Lx-x,Ly-y); 

end
disp('***************************')
disp('Description: ')
fprintf(description)
disp('')
disp('***************************')
end

