%%%  This script deals with the resolution of the two-species non-local SKT 
%%%  system discretized with a two-point flux appoximation of the equation
%%%  written in Laplacian form
%%%
%%%  d_t u1 - d_xx( (d1 + d12 rho1*u2 + d11 sig1*u1) u1) = R1(u1,u2)
%%%  d_t u2 - d_xx( (d2 + d21 rho2*u1 + d22 sig2*u2) u2) = R2(u1,u2)
%%%
%%%  The convolution kernels must satisfy rho_1(x) = rho_2(-x) =:rho(x) and 
%%%  sig_i(x) = sig_i(-x) for i = 1,2 in order for the system to satisfy an
%%%  entropy - entropy disspation inequality. But other convolution kernels
%%%  may be used
%%%
%%%  The domain (R / LxZ)\times(R / LyZ) is discretized with Nx*Ny
%%%  points. The time discretization is fully implicit.
%%%
%%%  Preprint available at https://arxiv.org/abs/2207.01928
%%%
%%%  Author: Maxime Herda (maxime.herda@nia.fr) 

clc
clear all
close all
addpath MatricesAssembling/
addpath Newton/
addpath Diverse/
addpath ReactionTerms/
addpath textprogressbar/
rng(1234567891) % fixing random seed


%% Options

testcase = 2; % Consult or modify the loadTestcase2d.m file to choose or change the test case

UseParallel = true;% Parallelizes the assembling of the matrices
doSparse = true;% Uses sparse matrices
checkJac = 1; % Verifies that the Jacobian is well computed
checkSparse = 1; % Verifies that the Jacobian is a sparse matrix
showNonZeroCoefficients = 0; % shows which coefficients may not be well computed
plotsolution = 1;% Plots the solution at each time step
recordVideo = 0;% Saves plot of the solution on video
convergenceTest = 0; % 1: solve the equation for various time step and plots "error vs. time step"
longTime = 0; % Plots the evolution of the entropy in time.
quiet = 1;

%% Space time domain

Lx = 4;% Horizontal length of the domain
Ly = 3;% Vertical length of the domain
T = 20;% Final time

%% Load parameters of the model

[d1,d2,d12,d21,d11,d22, rho1func, rho2func, sig1func, sig2func, reacfunc, reacJac, u1func, u2func, description] = loadTestcase2d(testcase,Lx,Ly,doSparse);

%% Mesh

if convergenceTest
    dt0 = T;
    N0 = 2;
    Nrefine = 4;
    dtVec = dt0*(0.25).^(0:Nrefine-1);% Time step
    NVec = N0*(2).^(1:Nrefine);% Number of cells
    approxSolutions = cell(1,Nrefine);% to store the numerical result
else
    Nrefine = 1;
    dtVec = [.01];% Time step
    NVec = 40;% Number of cells
end


for i = 1:Nrefine
    dt = dtVec(i);
    Nx = floor(NVec(i)*Lx/Ly);
    Ny = NVec(i);
    N = Nx*Ny;

    dx = Lx/Nx; % Horizontal mesh size
    dy = Ly/Ny; % Vertical mesh size
    x = (0:(Nx-1))'*dx; % Cell centers, horizontal coordinates
    y = (0:(Ny-1))'*dy; % Cell centers, vertical coordinates 
    
    
    xplot = (0:Nx)'*dx; % Cell centers, horizontal coordinates
    yplot = (0:Ny)'*dy; % Cell centers, vertical coordinates 
    
    %%% Discretization in matrix form (for plots etc..)
    % x varies along columns and y along lines
    [X,Y] = meshgrid(x,y);
    [XPLOT,YPLOT] = meshgrid(xplot,yplot);
    
    

    %% Pre-assembling  matrices
    %%% Convolution kernels in matrix and vector forms
    RHO1 = rho1func(X,Y);
    RHO1 = RHO1 / sum(sum(RHO1*dx*dy));
    RHO2 = rho2func(X,Y);
    RHO2 = RHO2 / sum(sum(RHO2*dx*dy));

    rho1 = reshape(RHO1', N, 1); % Lexicographical goes through abscissas first
    rho2 = reshape(RHO2', N, 1);
    
    
    SIG1 = sig1func(X,Y);
    SIG1 = SIG1 / sum(sum(SIG1*dx*dy));
    SIG2 = sig2func(X,Y);
    SIG2 = SIG2 / sum(sum(SIG2*dx*dy));

    sig1 = reshape(SIG1', N, 1); % Lexicographical goes through abscissas first
    sig2 = reshape(SIG2', N, 1);


    %%% Initial data 
    U1 = u1func(X,Y);
    U2 = u2func(X,Y);
    u1 = reshape(U1', N, 1);
    u2 = reshape(U2', N, 1);
    u = [u1;u2];
    uinit = u;



    %%% Assembling matrices which will be used at each time step:
    % A: discrete Laplacian
    % rho1Conv: discrete convolution with rho1
    % rho2Conv: discrete convolution with rho2
    % sig1Conv: discrete convolution with sig1
    % sig2Conv: discrete convolution with sig2

    disp('Pre-assembling matrices')
    [Ax, Ay, rho1Conv, rho2Conv, sig1Conv, sig2Conv] = Assemble2d(Nx, Ny, rho1, rho2, sig1, sig2, RHO1, RHO2, SIG1, SIG2,  UseParallel, doSparse);


    %% Verifications concerning the computation of the Jacobian
    
    disp('***************************')
    disp('Verifications concerning the computation of the Jacobian')
    if checkJac
        checkJacobian2d(showNonZeroCoefficients, RHO1, RHO2, SIG1, SIG2, rho1Conv, rho2Conv, sig1Conv, sig2Conv, Ax, Ay, dx, dy, dt, Nx, Ny, N, d12, d21, d11, d22, d1, d2, reacfunc, reacJac, doSparse);
    end
    if checkSparse
        checkSparsefunc2d(RHO1, RHO2, SIG1, SIG2, rho1Conv, rho2Conv, sig1Conv, sig2Conv, Ax, Ay, dx, dy, dt, Nx, Ny, N, d12, d21, d11, d22, d1, d2, reacfunc, reacJac, doSparse)
    end
    disp('***************************')


    %% Initiating figures for plots

    Fontsize = 18;

    if plotsolution % Figure for plotting the solution 
        fig1 = figure(1);
        set(gca,'fontsize', Fontsize,'TickLabelInterpreter', 'latex')
        colormap default
        if recordVideo
            description2 = description(1:end-2);
            videotitle = sprintf('vid_%s.avi', datestr(now,'mm-dd-yyyy_HH-MM'));
            vid = VideoWriter(videotitle,'Uncompressed AVI');
            open(vid);
        end
    end
    if convergenceTest % Figure for plotting convergence curve
        fig2 = figure(2);
        set(gca,'fontsize', Fontsize,'TickLabelInterpreter', 'latex')
    end
    if longTime % Figure for plotting long time behavior of the solution 
        fig3 = figure(3);
        set(gca,'fontsize', Fontsize,'TickLabelInterpreter', 'latex')
    end

    %% Time loop
    
    t = 0;
    Vt = [0];
    dtmax = dt;
    newdt = dt;
    Ent = [d21*sum(boltz(u1)*dx*dy) + d12*sum(boltz(u2)*dx*dy)];

    textprogressbar(['Number of cells in the mesh = ',num2str(N),'. Progress:   ']) % Initialize progress bar
    
    while t< T
        
        %%% Plot of the solution 
        if plotsolution
            set(0, 'currentfigure', fig1);
            UPLOT1 = U1./max(u1);
            UPLOT1 = [UPLOT1; UPLOT1(1,:)];
            UPLOT1 = [UPLOT1, UPLOT1(:,1)];
            
            
            surf(XPLOT,YPLOT,UPLOT1)
            title(['$u_1/\max(u_1)$ at $t=',num2str(t(end)),'$'],'Interpreter','latex')
            axis square
            xlabel('$x$','Interpreter','latex')
            ylabel('$y$','Interpreter','latex')
            xlim([0,Lx])
            ylim([0,Ly])
            caxis([0,1])
            set(gca,'xtick',[])
            set(gca,'ytick',[])
            grid off
            view(2)
            daspect([1 1 1])
            shading interp
            colorbar
            hold off
            drawnow
            if recordVideo
                frame = getframe(gcf);
                writeVideo(vid,frame);
            end
        end
        
        %%% Computes the soltion of the scheme
        [u,newdt] = SolveSyst2d(u, newdt, dtmax, rho1Conv, rho2Conv, sig1Conv, sig2Conv, RHO1, RHO2, SIG1, SIG2, Ax, Ay, dx, dy, Nx, Ny, d12, d21, d11, d22, d1,d2, reacfunc,reacJac, doSparse, quiet);
        textprogressbar(t/T*100) % Update progress bar
        u1 = u(1:N);
        u2 = u(N+1:2*N);
        U1 = reshape(u1,Nx,Ny)';
        U2 = reshape(u2,Nx,Ny)';
        Ent = [Ent,d21*sum(boltz(u1)*dx*dy) + d12*sum(boltz(u2)*dx*dy)];
        t = t+newdt;
        Vt = [Vt,t];
    end

    textprogressbar(100)
    textprogressbar('done') % Terminate progress bar
    
    if recordVideo
        close(vid);
    end
    
    
    %%% Plot entropy along time
    if longTime
        set(0, 'currentfigure', fig3);
        semilogy(Vt, Ent)
        title('Entropy along time','Interpreter','latex')
        xlabel('$t$','Interpreter','latex')
        ylabel('Entropy','Interpreter','latex')
        hold on
        drawnow
    end
    %%% Store data for convergence curves
    if convergenceTest
        approxSolutions(i) = {u};
    end
end



