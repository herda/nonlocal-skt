function DPhiD = DPhiDiff(A,dx,dt,d1,d2)
%DPHIDIFF Jacobian of the linear diffusion part

    DPhiD = dt * blkdiag(d1*A,d2*A) / (dx*dx);

end

