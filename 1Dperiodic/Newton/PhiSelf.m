function [PhiS,muS1,muS2] = PhiSelf(u1, u2, sig1, sig2, A, dx, dt, d11, d22)
%PHISELF Function of the self-diffusion part

    [muS1, muS2] = ConvSelf(u1, u2, sig1, sig2, dx);
    
    ALapl = dt*A/(dx*dx);
    PhiS = [d11*ALapl*(u1.*muS1); d22*ALapl*(u2.*muS2)];

end

