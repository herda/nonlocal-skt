function DP = DPhi(u, muC1, muC2, muS1, muS2, rho1Conv, rho2Conv, sig1Conv, sig2Conv, A, dx, dt, N, d12, d21, d11, d22, d1, d2, reacJac, doSparse)
%DPHI Jacobian of the whole scheme

    u1 = u(1:N);
    u2 = u((N+1):end);
    
    DPhiS = DPhiSelf(u1, u2, muS1, muS2, sig1Conv, sig2Conv, A, dx, dt, d11, d22, doSparse);
    DPhiC = DPhiCross(u1, u2, muC1, muC2, rho1Conv, rho2Conv, A, dx, dt, d12, d21, doSparse);
    DPhiD = DPhiDiff(A,dx,dt,d1,d2);
    DPhiR = DPhiReac(u1, u2, dt, reacJac);
    
    if doSparse
        DP = speye(length(u)) + DPhiC + DPhiS + DPhiD + DPhiR;
    else
        DP = eye(length(u)) + DPhiC + DPhiS + DPhiD + DPhiR;
    end
    
end

