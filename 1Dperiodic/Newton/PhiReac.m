function PhiR = PhiReac(u1, u2, dt, reacfunc)
%PHIREAC Function of the reaction part

   
    PhiR = -dt*reacfunc(u1,u2);

end

