function [PhiC,muC1,muC2] = PhiCross(u1, u2, rho1, rho2, A, dx, dt, d12, d21)
%PHICROSS Function of the cross-diffusion part

    [muC1, muC2] = ConvCross(u1, u2, rho1, rho2, dx);
    
    ALapl = dt*A/(dx*dx);
    PhiC = [d12*ALapl*(u1.*muC1); d21*ALapl*(u2.*muC2)];

end

