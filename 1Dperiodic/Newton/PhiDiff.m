function PhiD = PhiDiff(u1, u2, A, dx, dt, d1, d2)
%PHIDIFF Function of the linear diffusion part


   ALapl = dt*A/(dx*dx);
   PhiD = [d1*ALapl*u1; d2*ALapl*u2];

end

