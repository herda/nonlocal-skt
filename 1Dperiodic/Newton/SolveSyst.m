function [u,newdt] = SolveSyst(un, dt, dtmax, rho1Conv, rho2Conv, sig1Conv, sig2Conv, rho1, rho2, sig1, sig2, A, dx, N, d12, d21, d11, d22, d1, d2, reacfunc, reacJac, doSparse, quiet)
%NEWTON Resolution of one time step of the scheme with a Newton method.
%
%There is an adaptive time step procedure. It is increased if less than dtmax and decreased if the Newton method fails to converge. 
%
%   INPUT:
%
%   un: solution at previous time
%   dt: time step
%   dtmax: maximal allowed time step
%   A: discrete Laplacian with periodic BC
%   rho1Conv,rho2Conv, sig1Conv, sig2Conv: discrete convolution matrices
%   rho1, rho2, sig1, sig2: convolution kernels 
%   reacfunc, reacJac: functions computing reaction term and its Jacobian
%   d12, d21, d11, d22, d1, d2: diffusion coefficients
%   doSparse: uses sparse matrices
%   quiet: more infos and alerts if true
%
%   OUTPUT:
%
%   u: solution of the scheme
%   newdt: new time step

maxit = 50; % Maximum number of Newton iteration
targeterror = 1e-10; % Target error (relative) to stop Newton iterations

err = Inf;

newdt = min(2*dt,dtmax); % Increasing time step

if newdt > dt
    disp('Unrefine')
end

while err > targeterror
    u = un;
    it = 0;
    [Phitmp,muC1,muC2, muS1, muS2] = Phi(un, u, rho1, rho2, sig1, sig2, A, dx, dt, N, d12, d21, d11, d22, d1, d2, reacfunc); % Function at initial guess  
    while (it < maxit) && (err > targeterror)
        DPhitmp = DPhi(u, muC1, muC2, muS1, muS2, rho1Conv, rho2Conv, sig1Conv, sig2Conv, A, dx, dt, N, d12, d21, d11, d22, d1, d2, reacJac, doSparse);% Jacobian at new iterate 
        res = DPhitmp\Phitmp; % Residue
        u = u-res; % New guess
        u = max(u,0); % Projection to avoid instability due to negative floating point error 
        [Phitmp,muC1,muC2, muS1, muS2] = Phi(un, u, rho1, rho2, sig1, sig2, A, dx, dt, N, d12, d21, d11, d22, d1, d2, reacfunc);% Function at new iterate  
        err = norm(res,Inf)/norm(un,Inf); % Error (relative) between new and last iterate
        it = it + 1;
    end
    if it == maxit && (err > targeterror) % Time step refinement if fails to converge
        newdt = newdt/2;
        disp('Refine')
    end
    if ~quiet
        disp([num2str(it),' Newton iterations'])
    end
end

