function DPhiS = DPhiSelf(u1, u2, muS1, muS2, sig1Conv, sig2Conv, A, dx, dt, d11, d22, doSparse)
%DPHISELF Jacobian of the self diffusion part

    ALapl = dt*A/(dx*dx);
    ALapl2 = ALapl*dx;

    if doSparse
        DPhiS11 = d11*ALapl*spdiags(muS1,0,length(muS1),length(muS1));
        DPhiS22 = d22*ALapl*spdiags(muS2,0,length(muS2),length(muS2));
        DPhiS12 = d11*ALapl2*spdiags(u1,0,length(u1),length(u1))*sig1Conv;
        DPhiS21 = d22*ALapl2*spdiags(u2,0,length(u2),length(u2))*sig2Conv;
    else
        DPhiS11 = d11*ALapl*diag(muS1);
        DPhiS22 = d22*ALapl*diag(muS2);
        DPhiS12 = d11*ALapl2*diag(u1)*sig1Conv;
        DPhiS21 = d22*ALapl2*diag(u2)*sig2Conv;
    end
    

    DPhiS = blkdiag(DPhiS11+DPhiS12,DPhiS21+DPhiS22);

end

