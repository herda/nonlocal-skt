function [P,muC1,muC2, muS1, muS2] = Phi(un, u, rho1, rho2, sig1, sig2, A, dx, dt, N, d12, d21, d11, d22, d1, d2, reacfunc)
%PHI Nonlinear function defining the scheme

    u1 = u(1:N);
    u2 = u(N+1:end);
    
    [PhiS,muS1,muS2] = PhiSelf(u1, u2, sig1, sig2, A, dx, dt, d11, d22);
    [PhiC,muC1,muC2] = PhiCross(u1, u2, rho1, rho2, A, dx, dt, d12, d21);
    PhiD = PhiDiff(u1, u2, A, dx, dt, d1, d2);
    PhiR = PhiReac(u1, u2, dt, reacfunc);
     
    P = u + PhiS + PhiC + PhiD + PhiR - un;

end

