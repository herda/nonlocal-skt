function out = ReacCompet(u,v,a1,a2,b1,b2,K1,K2,Jac,doSparse)
%REACCOMPET Lotka Volterra with competition between species

if Jac % Evaluation of the Jacobian of the function
    N = length(u);
    if doSparse
        out = [[a1*speye(N) - 2*a1*spdiags(u,0,N,N)/K1 - b1*spdiags(v,0,N,N)/K1, - b1*spdiags(u,0,N,N)/K1]; [- b2*spdiags(v,0,N,N)/K2, a2*speye(N) - 2*a2*spdiags(v,0,N,N)/K2 - b2*spdiags(u,0,N,N)/K2]];
    else
        out = [[a1*eye(N) - 2*a1*diag(u)/K1 - b1*diag(v)/K1, - b1*diag(u)/K1]; [- b2*diag(v)/K2, a2*eye(N) - 2*a2*diag(v)/K2 - b2*diag(u)/K2]];
    end
else % Evaluation of the function
    out = [a1*u.*(K1-u-b1*v)/K1; a2*v.*(K2-v-b2*u)/K2];
end
end

