function out = ReacBrentnall(u1,u2,b,v,g,d,Jac,doSparse)
%REACBrentnall
% 
if Jac % Evaluation of the Jacobian of the function
    N = length(u1);
    if doSparse
        out = [[spdiags((2.*u1.^3.*u2)./(u1.^2 + v.^2).^2 - b.*(u1 - 1) - b.*u1 - (2.*u1.*u2)./(u1.^2 + v.^2),0,N,N), spdiags(-u1.^2./(u1.^2 + v.^2),0,N,N)]; [spdiags(-g.*((2.*u1.^3.*u2)./(u1.^2 + v.^2).^2 - (2.*u1.*u2)./(u1.^2 + v.^2)),0,N,N), spdiags(g.*(u1.^2./(u1.^2 + v.^2) - d.^2./(d.^2 + 1)),0,N,N)]];
    else
        out = [[diag((2.*u1.^3.*u2)./(u1.^2 + v.^2).^2 - b.*(u1 - 1) - b.*u1 - (2.*u1.*u2)./(u1.^2 + v.^2)), diag(-u1.^2./(u1.^2 + v.^2))]; [diag(-g.*((2.*u1.^3.*u2)./(u1.^2 + v.^2).^2 - (2.*u1.*u2)./(u1.^2 + v.^2))), diag(g.*(u1.^2./(u1.^2 + v.^2) - d.^2./(d.^2 + 1)))]];
    end
else % Evaluation of the function
    out = [b.*u1.*(1-u1) - u2.*u1.*u1./(v.*v+u1.*u1); g.*(b.*u2.*u1.*u1./(v.*v+u1.*u1) - d.*d./(1+d.*d).*u2)];
end

end

