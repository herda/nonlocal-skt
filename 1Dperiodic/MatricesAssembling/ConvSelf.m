function [mu1S, mu2S] = ConvSelf(u1, u2, sig1, sig2, dx)
%CONVSELF  computes the convolution products in the self-diffusion term

    mu1S = dx * ifft(fft(sig1).*fft(u1));
    mu2S = dx * ifft(fft(sig2).*fft(u2));

end

