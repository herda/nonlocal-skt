function [mu1C, mu2C] = ConvCross(u1, u2, rho1,rho2,dx)
%CONVCROSS computes the convolution products in the cross-diffusion term

    mu1C = dx * ifft(fft(rho1).*fft(u2));
    mu2C = dx * ifft(fft(rho2).*fft(u1));

end

