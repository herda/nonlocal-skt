function [A, rho1Conv, rho2Conv, sig1Conv, sig2Conv]...
    = Assemble(N, rho1, rho2, sig1, sig2,  UseParallel, doSparse)
%ASSEMBLE Assembles matrices which are going to be used repeatedly in the
%computations
%
%   INPUT:
%
%   N: size of the vector of unknowns
%   rho1: cross convolution kernel for species 1
%   rho2: cross convolution kernel for species 2
%   sig1: self convolution kernel for species 1
%   sig2: self convolution kernel for species 2
%   UseParallel: parallelizes the assembling (option of func2mat routine)
%   doSparse: uses sparse matrices
%
%   OUTPUT:
%
%   A: discrete Laplacian with periodic BC
%   rho1Conv: discrete convolution with rho1
%   rho2Conv: discrete convolution with rho2
%   sig1Conv: discrete convolution with sig1
%   sig2Conv: discrete convolution with sig2



multiplx = 4*diag(sin(pi*(0:(N-1))/N).^2);
A = round(real(ifft(multiplx*fft(eye(N)))));
if doSparse
    A = sparse(A);
end

tol = 1e-12;

rho1Conv = @(X) ifft(fft(rho1).*fft(X)); 
rho1Conv = @(X) rho1Conv(X).*(abs(rho1Conv(X))>tol);
rho1Conv = func2mat(rho1Conv,rho1,'UseParallel',UseParallel, 'doSparse', doSparse);

rho2Conv = @(X) ifft(fft(rho2).*fft(X)); 
rho2Conv = @(X) rho2Conv(X).*(abs(rho2Conv(X))>tol);
rho2Conv = func2mat(rho2Conv,rho2,'UseParallel',UseParallel, 'doSparse', doSparse);

sig1Conv = @(X) ifft(fft(sig1).*fft(X)); 
sig1Conv = @(X) sig1Conv(X).*(abs(sig1Conv(X))>tol);
sig1Conv = func2mat(sig1Conv,sig1,'UseParallel',UseParallel, 'doSparse', doSparse);

sig2Conv = @(X) ifft(fft(sig2).*fft(X)); 
sig2Conv = @(X) sig2Conv(X).*(abs(sig2Conv(X))>tol);
sig2Conv = func2mat(sig2Conv,sig2,'UseParallel',UseParallel, 'doSparse', doSparse);


end

