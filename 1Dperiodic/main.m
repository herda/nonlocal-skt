%%%  This script deals with the resolution of the two-species non-local SKT 
%%%  system discretized with a two-point flux appoximation of the equation
%%%  written in Laplacian form
%%%
%%%  d_t u1 - d_xx( (d1 + d12 rho1*u2 + d11 sig1*u1) u1) = R1(u1,u2)
%%%  d_t u2 - d_xx( (d2 + d21 rho2*u1 + d22 sig2*u2) u2) = R2(u1,u2)
%%%
%%%  The convolution kernels must satisfy rho_1(x) = rho_2(-x) =:rho(x) and 
%%%  sig_i(x) = sig_i(-x) for i = 1,2 in order for the system to satisfy an
%%%  entropy - entropy disspation inequality. But other convolution kernels
%%%  may be used
%%%
%%%  The domain R / LZ is periodic with length L and is discretized with N
%%%  points. The time discretization is fully implicit.
%%%
%%%  Preprint available at https://arxiv.org/abs/2207.01928
%%%
%%%  Author: Maxime Herda (maxime.herda@nia.fr) 

clear all
close all
addpath MatricesAssembling/
addpath Newton/
addpath Diverse/
addpath ReactionTerms/
addpath textprogressbar/
addpath matlab2tikz/


%% Options

testcase = 2; % Consult or modify the loadTestcase.m file to choose or change the test case

UseParallel = false;% Parallelizes the assembling of the matrices
doSparse = true;% Uses sparse matrices
checkJac = 1; % Verifies that the Jacobian is well computed
checkSparse = 1; % Verifies that the Jacobian is a sparse matrix
showNonZeroCoefficients = 0; % shows which coefficients may not be well computed
plotsolution = 1;% Plots the solution at each time step
saveTikzLastPlot = 0;% For saving last density plot in a file
convergenceTest = 0; % 1: solve the equation for various time step and plots "error vs. time step"
longTime = 0; % Plots the evolution of the entropy in time
quiet = 1;

%% Space time domain

L = 25;% Length of the domain
T = 10;% Final time

%% Load parameters of the model

[d1,d2,d12,d21,d11,d22, rho1func, rho2func, sig1func, sig2func, reacfunc, reacJac, u1func, u2func,description] =  loadTestcase(testcase,L,doSparse);

%% Mesh

if convergenceTest
    dt0 = T;
    N0 = 32;
    Nrefine = 6;
    dtVec = dt0*(0.25).^(0:Nrefine-1);% Time step
    NVec = N0*(2).^(0:Nrefine-1);% Number of cells
    approxSolutions = cell(1,Nrefine);% to store the numerical result
else
    Nrefine = 1;
    dtVec = [.01];% Time step
    NVec = 256;% Number of cells
end


for i = 1:Nrefine
    dt = dtVec(i);
    N = NVec(i);

    dx = L/N; % Mesh size
    x = (0:(N-1))'*dx; % Cell centers

    %% Pre-assembling  matrices
    %%% Convolution kernels
    rho1 = rho1func(x);
    rho2 = rho2func(x);
    rho1 = rho1 /sum(rho1*dx);
    rho2 = rho2 /sum(rho2*dx);

    sig1 = sig1func(x);
    sig2 = sig2func(x);
    sig1 = sig1 /sum(rho1*dx);
    sig2 = sig2 /sum(rho2*dx);


    %%% Initial data 
    u1 = u1func(x);
    u2 = u2func(x);
    u = [u1;u2];
    uinit = u;


    %%% Assembling matrices which will be used at each time step:
    % A: discrete Laplacian
    % rho1Conv: discrete convolution with rho1
    % rho2Conv: discrete convolution with rho2
    % sig1Conv: discrete convolution with sig1
    % sig2Conv: discrete convolution with sig2

    disp('Pre-assembling matrices')
    [A, rho1Conv, rho2Conv, sig1Conv, sig2Conv] = Assemble(N, rho1, rho2, sig1, sig2,  UseParallel, doSparse);

    %% Verifications concerning the computation of the Jacobian
    
    disp('***************************')
    disp('Verifications concerning the computation of the Jacobian')
    if checkJac
        checkJacobian(showNonZeroCoefficients, rho1, rho2, sig1, sig2, rho1Conv, rho2Conv, sig1Conv, sig2Conv, A, dx, dt, N, d12, d21, d11, d22, d1, d2, reacfunc, reacJac, doSparse);
    end
    if checkSparse
        checkSparsefunc(rho1, rho2, sig1, sig2, rho1Conv, rho2Conv, sig1Conv, sig2Conv, A, dx, dt, N, d12, d21, d11, d22, d1, d2, reacfunc, reacJac, doSparse)
    end
    disp('***************************')


    %% Initiating figures for plots

    Fontsize = 18;

    if plotsolution || saveTikzLastPlot % Figure for plotting the solution 
        fig1 = figure(1);
        set(gca,'fontsize', Fontsize,'TickLabelInterpreter', 'latex')
    end
    if convergenceTest % Figure for plotting convergence curve
        fig2 = figure(2);
        set(gca,'fontsize', Fontsize,'TickLabelInterpreter', 'latex')
    end
    if longTime % Figure for plotting long time behavior of the solution 
        fig3 = figure(3);
        set(gca,'fontsize', Fontsize,'TickLabelInterpreter', 'latex')
    end

    %% Time loop
    
    t = 0;
    Vt = [0];
    dtmax = dt;
    newdt = dt;
    Ent = [d21*sum(boltz(u1)*dx) + d12*sum(boltz(u2)*dx)];

    textprogressbar(['Number of cells in the mesh = ',num2str(N),'. Progress:   ']) % Initialize progress bar
    while t< T
        
        %%% Plot of the solution 
        if plotsolution
            set(0, 'currentfigure', fig1);
            plot(x,u1)
            hold on
            plot(x,u2)
            legend({'u1','u2'},'Interpreter','latex')
            title(['$t=',num2str(t(end)),'$', '     $min(u1) = ', num2str(min(u1)), '$     $min(u2) = ', num2str(min(u2)),'$'],'Interpreter','latex')
            axis square
            xlabel('$x$','Interpreter','latex')
            xlim([0,L])
            hold off
            drawnow
        end
        
        %%% Computes the soltion of the scheme
        [u,newdt] = SolveSyst(u, newdt, dtmax, rho1Conv, rho2Conv, sig1Conv, sig2Conv, rho1, rho2, sig1, sig2, A, dx, N, d12, d21, d11, d22, d1, d2, reacfunc, reacJac, doSparse, quiet);
        textprogressbar(t/T*100) % Update progress bar
        u1 = u(1:N);
        u2 = u(N+1:2*N);
        Ent = [Ent,d21*sum(boltz(u1)*dx) + d12*sum(boltz(u2)*dx)];
        t = t+newdt;
        Vt = [Vt,t];
    end

    textprogressbar(100)
    textprogressbar('done') % Terminate progress bar
    
    %%% Plot entropy along time
    if longTime
        set(0, 'currentfigure', fig3);
        semilogy(Vt, Ent)
        title('Entropy along time','Interpreter','latex')
        xlabel('$t$','Interpreter','latex')
        ylabel('Entropy','Interpreter','latex')
        hold on
        drawnow
    end
    %%% Store data for convergence curves
    if convergenceTest
        approxSolutions(i) = {u};
    end
end


%% Saving last density plot in a file
if saveTikzLastPlot
    set(0, 'currentfigure', fig1);
    plot(x,u1)
    hold on
    plot(x,u2)
    legend({'u1','u2'},'Interpreter','latex')
    title(['$t=',num2str(t(end)),'$', '     $min(u1) = ', num2str(min(u1)), '$     $min(u2) = ', num2str(min(u2)),'$'],'Interpreter','latex')
    axis square
    xlabel('$x$','Interpreter','latex')
    xlim([0,L])
    hold off
    drawnow
    set(0, 'currentfigure', fig1);
    filename = sprintf('fig_%s.tex', datestr(now,'mm-dd-yyyy_HH-MM'));
    matlab2tikz('filename',filename);
end


%% Convergence curves
if convergenceTest
    errLinf = zeros(1,Nrefine-1);
    dxVec = L./NVec(1:end-1);
    refsol = approxSolutions(end);
    for i = 1:Nrefine-1
        sol = approxSolutions(i);
        errLinf(i) = max(abs(sol{:} -refsol{:}(1:2^(Nrefine-i):end)));
    end
    set(0, 'currentfigure', fig2);
    loglog(dxVec, errLinf,'r-*')
    hold on 
    loglog(dxVec, dxVec.^2,'b--')
    [order, ~] = polyfit(log(dxVec), log(errLinf),1);
    disp(['Order of convergence is estimated to be ', num2str(order(1))])
    title('Convergence plot','Interpreter','latex')
    xlabel('$\Delta x$','Interpreter','latex')
    ylabel('$L^\infty$ error with reference solution','Interpreter','latex')
    legend('Error','Order 2')
end





