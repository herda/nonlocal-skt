function [d1,d2,d12,d21,d11,d22, rho1func, rho2func, sig1func, sig2func, reacfunc, reacJac, u1func, u2func, description] = loadTestcase(testcase,L,doSparse)
%LOADTESTCASE Loads the parameters of the system of equations
%
%   INPUT:
%
%   testcase: identification number of the example (see below in the code of this function for details)
%   L: size of the domain
%   doSparse: option to use only sparse matrices (recommanded)
%
%   OUTPUT:
%
%   d1,d2,d12,d21,d11,d22: diffusion coefficients
%   rho1func, rho2func, sig1func, sig2func: convolution kernels
%   reacfunc, reacJac: function for reaction term and its Jacobian
%   u1func, u2func: initial data


disp('***************************')
disp(['Test case ',num2str(testcase)])

%%% Default values (2 uncoupled heat equations)

description = "Two decoupled heat equations\n";
d1=1;d2=1;d12=0;d21=0;d11=0;d22=0; % Only linear diffusion
rho1func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
rho2func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
sig1func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
sig2func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
reacfunc = @(u1,u2) 0*[u1;u2]; % No reaction
if doSparse
    reacJac = @(u1,u2) 0*spdiags([u1;u2],0,length([u1;u2]),length([u1;u2])); % No reaction
else
    reacJac = @(u1,u2) 0*diag([u1;u2]); % No reaction    
end
u1func = @(x) ((x>= L/9).*(x<=L/3));
u2func = @(x) sin(2*pi*x/L)+1;


%%% Testcases
switch testcase
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 1 % Turing instability linear diffusion
        
        description = "Turing instability in Segel-Levin with linear diffusion\n";

        %%% Coefficients
        % Diffusion coefficients
        d1 = .05;
        d2 = 2;
        % Cross-diffusion coefficients
        d12 = 0;
        d21 = 0;
        % Self-diffusion coefficients
        d11 = 0;
        d22 = 0;

       
        %%% Reaction term
        a = 1; % birth rate for preys (linear term)
        b = 1; % death rate for preys per predator
        c = 1; % birth rate for predators per prey
        d = 1; % birth rate for prey (quadratic term)
        e = 1/3; % death rate for predators (quadratic term)


        reacfunc = @(u1,u2) ReacSL(u1,u2,a,b,c,d,e,0);
        reacJac = @(u1,u2) ReacSL(u1,u2,a,b,c,d,e,1,doSparse);

        eta = .1;
        perturb = @(x) ((x>= L/9).*(x<=L/3));

        u1func = @(x) (a*d)/(b*c - d*e) + eta*perturb(x);
        u2func = @(x) (a*c)/(b*c - d*e) + 0*x;

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 2 % Turing instability cross-diffusion
        
        description = "Turing instability in Segel-Levin with nonlocal cross-diffusion\n";

        %%% Coefficients
        % Diffusion coefficients
        d1 = 0.05;
        d2 = 0;
        % Cross-diffusion coefficients
        d12 = 0;
        d21 = 1;
        % Self-diffusion coefficients
        d11 = 0;
        d22 = 0;

        %%% Convolution kernels
        % Cross diffusion
        wid = L/50;
        rad = L/4.9;
        predatorfunc = @(x) x.^2.*(x<= rad) + (x-2*rad).^2.*(x>rad).*(x<=2*rad); % rad<= L/4
        rho2func = @(x) predatorfunc(x) + predatorfunc(L-x);
        rho1func = @(x) (x<=wid) | (L-x<=wid);

        %%% Reaction term
        a = 1; % birth rate for preys (linear term)
        b = 1; % death rate for preys per predator
        c = 1; % birth rate for predators per prey
        d = 1; % birth rate for prey (quadratic term)
        e = 1/3; % death rate for predators (quadratic term)


        reacfunc = @(u1,u2) ReacSL(u1,u2,a,b,c,d,e,0, doSparse);
        reacJac = @(u1,u2) ReacSL(u1,u2,a,b,c,d,e,1, doSparse);

        eta = .1;
        perturb = @(x) ((x>= L/9).*(x<=L/3));

        u1func = @(x) (a*d)/(b*c - d*e) + eta*perturb(x);
        u2func = @(x) (a*c)/(b*c - d*e) + 0*x;
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 3 % For convergence test
        
        description = "Non-local cross-diffusion with smooth kernel and sine and cosine initial data\n";
        d1=0;d2=0;d12=1;d21=2;d11=0;d22=0; % Only linear diffusion
        sig1func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        sig2func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        rho1func = @(x) cos(2*pi*x/L)+1; % Smooth
        rho2func = @(x) cos(2*pi*x/L)+1; % Smooth
        reacfunc = @(u1,u2) 0*[u1;u2]; % No reaction
        if doSparse
            reacJac = @(u1,u2) 0*spdiags([u1;u2],0,length([u1;u2]),length([u1;u2])); % No reaction
        else
            reacJac = @(u1,u2) 0*diag([u1;u2]); % No reaction    
        end
        u1func = @(x) cos(2*pi*x/L)+1;
        u2func = @(x) sin(2*pi*x/L)+1;
        
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 4 % For convergence test
        
        description = "Non-local cross-diffusion with smooth kernel and indicator initial data\n";
        d1=0;d2=0;d12=1;d21=2;d11=0;d22=0; % Only linear diffusion
        sig1func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        sig2func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        rho1func = @(x) cos(2*pi*x/L)+1; % Smooth
        rho2func = @(x) cos(2*pi*x/L)+1; % Smooth
        reacfunc = @(u1,u2) 0*[u1;u2]; % No reaction
        if doSparse
            reacJac = @(u1,u2) 0*spdiags([u1;u2],0,length([u1;u2]),length([u1;u2])); % No reaction
        else
            reacJac = @(u1,u2) 0*diag([u1;u2]); % No reaction    
        end
        u1func = @(x) ((x>= L/9).*(x<=L/3));
        u2func = @(x)((x>= L/4).*(x<=3*L/4));
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 5 % For convergence test
        
        description = "Non-local cross-diffusion with indicatrix kernel and sine and cosine initial data\n";
        d1=0;d2=0;d12=1;d21=2;d11=0;d22=0; % Only linear diffusion
        sig1func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        sig2func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        wid = L/8;
        rho1func = @(x) (x<=wid) | (L-x<=wid); % Indicatrix
        rho2func = @(x) (x<=wid) | (L-x<=wid); % Indicatrix
        reacfunc = @(u1,u2) 0*[u1;u2]; % No reaction
        if doSparse
            reacJac = @(u1,u2) 0*spdiags([u1;u2],0,length([u1;u2]),length([u1;u2])); % No reaction
        else
            reacJac = @(u1,u2) 0*diag([u1;u2]); % No reaction    
        end
        u1func = @(x) cos(2*pi*x/L)+1;
        u2func = @(x) sin(2*pi*x/L)+1;
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 6 % For convergence test
        
        description = "Non-local cross-diffusion with indicatrix kernel and indicatrix  initial data\n";
        d1=0;d2=0;d12=1;d21=2;d11=0;d22=0; % Only linear diffusion
        sig1func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        sig2func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        wid = L/8;
        rho1func = @(x) (x<=wid) | (L-x<=wid); % Indicatrix
        rho2func = @(x) (x<=wid) | (L-x<=wid); % Indicatrix
        reacfunc = @(u1,u2) 0*[u1;u2]; % No reaction
        if doSparse
            reacJac = @(u1,u2) 0*spdiags([u1;u2],0,length([u1;u2]),length([u1;u2])); % No reaction
        else
            reacJac = @(u1,u2) 0*diag([u1;u2]); % No reaction    
        end
        u1func = @(x) ((x>= L/9).*(x<=L/3));
        u2func = @(x)((x>= L/4).*(x<=3*L/4));
        
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 7 % For convergence test
        
        description = "Local cross-diffusion and sine and cosine initial data\n";
        d1=0;d2=0;d12=1;d21=2;d11=0;d22=0; % Only linear diffusion
        sig1func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        sig2func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        rho1func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        rho2func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        reacfunc = @(u1,u2) 0*[u1;u2]; % No reaction
        if doSparse
            reacJac = @(u1,u2) 0*spdiags([u1;u2],0,length([u1;u2]),length([u1;u2])); % No reaction
        else
            reacJac = @(u1,u2) 0*diag([u1;u2]); % No reaction    
        end
        u1func = @(x) cos(2*pi*x/L)+1;
        u2func = @(x) sin(2*pi*x/L)+1;
        

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 8 % For convergence test
        
        description = "Local cross-diffusion and indicatrix initial data\n";
        d1=0;d2=0;d12=1;d21=2;d11=0;d22=0; % Only linear diffusion
        sig1func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        sig2func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        rho1func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        rho2func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        reacfunc = @(u1,u2) 0*[u1;u2]; % No reaction
        if doSparse
            reacJac = @(u1,u2) 0*spdiags([u1;u2],0,length([u1;u2]),length([u1;u2])); % No reaction
        else
            reacJac = @(u1,u2) 0*diag([u1;u2]); % No reaction    
        end
        u1func = @(x) ((x>= L/9).*(x<=L/3));
        u2func = @(x)((x>= L/4).*(x<=3*L/4));
        
    
     case 9 % For non-local to local
        
        description = "Non-local cross-diffusion with indicatrix kernel and indicatrix initial data\n";
        d1=0;d2=0;d12=1;d21=2;d11=0;d22=0; % Only linear diffusion
        sig1func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        sig2func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        wid = L*.5*0;
        rho1func = @(x) (x<=wid) | (L-x<=wid); % Indicatrix
        rho2func = @(x) (x<=wid) | (L-x<=wid); % Indicatrix
        reacfunc = @(u1,u2) 0*[u1;u2]; % No reaction
        if doSparse
            reacJac = @(u1,u2) 0*spdiags([u1;u2],0,length([u1;u2]),length([u1;u2])); % No reaction
        else
            reacJac = @(u1,u2) 0*diag([u1;u2]); % No reaction    
        end
        u1func = @(x) ((x>= L/9).*(x<=L/3));
        u2func = @(x)((x>= L/4).*(x<=3*L/4));
        
     case 10 % For non-local to local
        
        description = "Non-local cross-diffusion with indicatrix kernel and smooth initial data\n";
        d1=0;d2=0;d12=1;d21=2;d11=0;d22=0; % Only linear diffusion
        sig1func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        sig2func = @(x) [1; zeros(length(x)-1,1)]; % Dirac
        wid = L*.5*0;
        rho1func = @(x) (x<=wid) | (L-x<=wid); % Indicatrix
        rho2func = @(x) (x<=wid) | (L-x<=wid); % Indicatrix
        reacfunc = @(u1,u2) 0*[u1;u2]; % No reaction
        if doSparse
            reacJac = @(u1,u2) 0*spdiags([u1;u2],0,length([u1;u2]),length([u1;u2])); % No reaction
        else
            reacJac = @(u1,u2) 0*diag([u1;u2]); % No reaction    
        end
        u1func = @(x) cos(2*pi*x/L)+1;
        u2func = @(x) sin(2*pi*x/L)+1;
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 11 % Brentnall reaction with linear and local self diffusion
        
        description = "Brentnall reaction with linear and local self diffusion\n";
        %%% Coefficients
        % Diffusion coefficients
        d1 = 1;
        d2 = 1;
        % Cross-diffusion coefficients
        d12 = 0;
        d21 = 0;
        % Self-diffusion coefficients
        d11 = 2;
        d22 = 2;

        %%% Reaction term
        b = 1; % Birth rate of preys (logistic term)
        v = 1; % Death rate by predation for preys is roughly 1/(v*v) when there are many preys
        g = 1; % Birth rate of predators is roughly g*b/(v*v) when there are many preys
        d = 1; % Death rate of predators

        reacfunc = @(u1,u2) ReacBrentnall(u1,u2,b,v,g,d,0,doSparse);
        reacJac = @(u1,u2) ReacBrentnall(u1,u2,b,v,g,d,1,doSparse);

        u1func = @(x) ((x>= L/9).*(x<=L/3));
        u2func = @(x)((x>= L/4).*(x<=3*L/4));
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
    case 12 % Lotka Volterra with competition between species, local self diffusion
        
        description = "Lotka Volterra with competition between species, local self diffusion\n";
        %%% Coefficients
        % Diffusion coefficients
        d1 = 0;
        d2 = 0;
        % Cross-diffusion coefficients
        d12 = 0;
        d21 = 0;
        % Self-diffusion coefficients
        d11 = 1;
        d22 = 2;

        %%% Reaction term
        a1 = 1;
        a2 = 1;
        b1 = 1;
        b2 = 1;
        K1 = 1;
        K2 = 1;
        
        
        reacfunc = @(u1,u2) ReacCompet(u1,u2,a1,a2,b1,b2,K1,K2,0,doSparse);
        reacJac = @(u1,u2) ReacCompet(u1,u2,a1,a2,b1,b2,K1,K2,1,doSparse);

        u1func = @(x) ((x>= L/9).*(x<=L/3));
        u2func = @(x)((x>= L/4).*(x<=3*L/4));
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
    case 13 % Lotka Volterra for prey and predator with local self diffusion
        
        description = "Lotka Volterra for prey and predator with local self diffusion\n";
        %%% Coefficients
        % Diffusion coefficients
        d1 = 0;
        d2 = 0;
        % Cross-diffusion coefficients
        d12 = 0;
        d21 = 0;
        % Self-diffusion coefficients
        d11 = 1;
        d22 = 2;

        %%% Reaction term
        a1 = 1;
        a2 = 1;
        b1 = 1;
        b2 = 1;
        
        
        reacfunc = @(u1,u2)  ReacLV(u1,u2,a1,a2,b1,b2,0,doSparse);
        reacJac = @(u1,u2)  ReacLV(u1,u2,a1,a2,b1,b2,1,doSparse);

        u1func = @(x) ((x>= L/9).*(x<=L/3));
        u2func = @(x)((x>= L/4).*(x<=3*L/4));
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
    case 14 % Generalized Lotka Volterra for prey and predator with local self diffusion
        
        description = "Generalized Lotka Volterra for prey and predator with local self diffusion\n";
        %%% Coefficients
        % Diffusion coefficients
        d1 = 0;
        d2 = 0;
        % Cross-diffusion coefficients
        d12 = 0;
        d21 = 0;
        % Self-diffusion coefficients
        d11 = 1;
        d22 = 2;

        %%% Reaction term
        a1 = 2;
        a2 = 1;
        b1 = 1;
        b2 = 1;
        k1 = 0;
        k2 = 0;
        A = 1;
        
        
        reacfunc = @(u1,u2)  ReacGLV(u1,u2,a1,a2,b1,b2, k1,k2, A,0,doSparse);
        reacJac = @(u1,u2)  ReacGLV(u1,u2,a1,a2,b1,b2, k1,k2, A,1,doSparse);

        u1func = @(x) ((x>= L/9).*(x<=L/3));
        u2func = @(x)((x>= L/4).*(x<=3*L/4));
    
    
end
disp('***************************')
disp('Description: ')
fprintf(description)
disp('')
disp('***************************')
end




