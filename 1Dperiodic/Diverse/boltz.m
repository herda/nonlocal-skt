function y = boltz(x)
%BOLTZ The Boltzmann entropy function extended to 1 for x<=0
    y = ones(size(x));
    y(x>0) = x(x>0).*log(x(x>0))-x(x>0)+1;

end

